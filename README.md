# MapGenerator

A small package with some generator for map. It's more to test the creation of simple map generator and to my ant simulation [PyGAco](https://www.gitlab.com/lostpy/pygaco).

I don't think I'll develop this project in the long term, it's just for fun.


## Installation

To install, juste `pip install` with git:

```
pip install git+https://www.gitlab.com/lostpy/mapgenerator
```

Optional dependencies:

 * [pygame][pygame]
 * [arcade][arcade]

## Usage

Import the package:
```
import mapgenerator
```

### Basics

Using a ready-to-use generator:

```python
from mapgenerator import CaveMapGeneratorSimple


# Create an instance of generator
generator = CaveMapGeneratorSimple(initial_fill_rate=0.6, map_size=(10, 10))

# Generate the map
generated_map = generator.generate(smooth_iterations=3)
print(*(line for line in generated_map), sep='\n`)
```

Create a custom generator:

```python
from mapgenerator import MapGenerator2D


class EmptyMap(MapGenerator2D):
    """Generate an empty map with border.
    """
    
    def smooth_map(self):
        self.map = [
            [
                0
                for _ in range(self.border_thickness, self.map_size[0] - self.border_thickness)
            ]
            for _ in range(self.border_thickness, self.map_size[1] - self.border_thickness)
        ]
```

### With [Pygame][pygame]

Using the simple cave generator:

```python
import pygame
from mapgenerator.pygame import CaveMap


pygame.init()

CaveMap.TileType.SIZE = 8
MAP_SIZE = (200, 130)
WINDOW_SIZE = (
    MAP_SIZE[0] * CaveMap.TileType.SIZE,
    MAP_SIZE[1] * CaveMap.TileType.SIZE,
)


screen = pygame.display.set_mode(WINDOW_SIZE)
walls = CaveMap(size=MAP_SIZE)
walls.generate(5)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            break

    screen.fill(pygame.Color("grey45"))
    walls.draw(screen)
    pygame.display.flip()

pygame.quit()
```


### With [Arcade][arcade]

Using the simple cave generator:

```python
from arcade import Window
import arcade

from mapgenerator.arcade import CaveMap


CaveMap.TileType.SIZE = 8
MAP_SIZE = (200, 130)
WINDOW_SIZE = (
    MAP_SIZE[0] * CaveMap.TileType.SIZE,
    MAP_SIZE[1] * CaveMap.TileType.SIZE
)
WINDOW_TITLE = "Arcade MapGenerator"


class MyWindow(Window):
    
    def __init__(self):
        super().__init__(WINDOW_SIZE[0], WINDOW_SIZE[1], title=WINDOW_TITLE)
        arcade.set_background_color(arcade.color.DIM_GRAY)
        self.map = None

    def setup(self):
        self.map = CaveMap(size=MAP_SIZE)
        self.map.generate(smooth_iterations=5)

    def on_update(self, delta_time: float):
        pass

    def on_draw(self):
        self.clear()
        self.map.draw()


if __name__ == '__main__':
    w = MyWindow()
    w.setup()
    w.run()
```

[pygame]: https://www.pygame.org/news
[arcade]: https://api.arcade.academy/en/latest/index.html

