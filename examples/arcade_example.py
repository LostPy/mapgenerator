from arcade import Window
import arcade

from mapgenerator.arcade import CaveMap


CaveMap.TileType.SIZE = 8
MAP_SIZE = (200, 130)
WINDOW_SIZE = (
    MAP_SIZE[0] * CaveMap.TileType.SIZE,
    MAP_SIZE[1] * CaveMap.TileType.SIZE
)
WINDOW_TITLE = "Arcade MapGenerator"


class MyWindow(Window):
    
    def __init__(self):
        super().__init__(WINDOW_SIZE[0], WINDOW_SIZE[1], title=WINDOW_TITLE)
        arcade.set_background_color(arcade.color.DIM_GRAY)
        self.map = None

    def setup(self):
        self.map = CaveMap(size=MAP_SIZE)
        self.map.generate(smooth_iterations=5)

    def on_update(self, delta_time: float):
        pass

    def on_draw(self):
        self.clear()
        self.map.draw()


if __name__ == '__main__':
    w = MyWindow()
    w.setup()
    w.run()

