from mapgenerator import CaveMapGeneratorSimple


SIZE_MAP = (10, 10)

generator = CaveMapGeneratorSimple(0.6, SIZE_MAP, border_thickness=1)
print(*(generator.generate()), sep="\n")

