import pygame
from mapgenerator.pygame import CaveMap


pygame.init()

CaveMap.TileType.SIZE = 8
MAP_SIZE = (200, 130)
WINDOW_SIZE = (
    MAP_SIZE[0] * CaveMap.TileType.SIZE,
    MAP_SIZE[1] * CaveMap.TileType.SIZE,
)


screen = pygame.display.set_mode(WINDOW_SIZE)
walls = CaveMap(size=MAP_SIZE)
walls.generate(5)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            break

    screen.fill(pygame.Color("grey45"))
    walls.draw(screen)
    pygame.display.flip()

pygame.quit()

