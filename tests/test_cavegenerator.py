import pytest

from mapgenerator import CaveMapGeneratorSimple


def test_cave_generator():
    generator = CaveMapGeneratorSimple(0.6, (20, 20), border_thickness=1)
    assert generator.total_tiles == 20 * 20
    
    generator.random_fill()
    assert sum(generator.map[0]) == 20
    assert sum(generator.map[-1]) == 20
    for line in generator.map:
        assert line[0] == 1 and line[-1] == 1

