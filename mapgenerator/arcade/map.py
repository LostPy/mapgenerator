"""A class to represent a map with a generator for a 
[arcade](https://api.arcade.academy/en/latest/index.html) application.

??? note "License"

    ```
    Copyright © 2022 LostPy <https://gitlab.com/LostPy>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
    ```
"""
from arcade import SpriteList
import arcade
from .. import MapGenerator2D, CaveMapGeneratorSimple
from . import Tile, CaveWallTile


class BaseMap(SpriteList):
    """Base class for a map with a generator. 
    It's a subclass of `arcade.SpriteList`.

    Attributes
    ----------
    size : tuple[int, int]
        Size of the map in tile number (! not in pixels).
    position : tuple[int, int]
        The bottom left corner postion of the map.
    generator : MapGenerator2D
        The map generator used to build the map.
    """

    TileType = Tile

    def __init__(self,
                 name: str,
                 generator: MapGenerator2D,
                 size: tuple[int, int],
                 position: tuple[int, int] = (0, 0)):
        super().__init__(name)
        self.size = size
        self.position = position
        self.generator = generator

    def generate(self, smooth_iterations: int = 1):
        """Method to generate the map with the generator.
        The default implementation generate a map with only one type of tile.
        
        You can customize this function with:
        
        ```python
        class MyMap(BaseMap):
            
            def generate(self, smooth_iterations: int = 1):
                self.clear()
                for y in range(...):
                    for x in range(...):
                        # code with `self.append(MyTile(x, y))`
                        ...
        ```

        Arguments
        ---------
        smooth_iterations : int, optional
            The number of smoothing operations to generate the map. Default: 1.
        """
        cls = self.__class__
        self.clear()
        generated_map = self.generator.generate(smooth_iterations)
        # reverse to start with the bottom of the map.
        for y in range(self.size[1] - 1, -1, -1):
            for x in range(0, self.size[0]):
                if generated_map[y][x] == 1:
                    self.append(cls.TileType(
                        x * cls.TileType.SIZE,
                        abs(y - self.size[1] + 1) * cls.TileType.SIZE,
                    ))

class CaveMap(BaseMap):
    """Class for Cave Map with the `CaveMapGeneratorSimple`.

    Attributes
    ----------
    size : tuple[int, int]
        Size of the map in tile number (! not in pixels).
    position : tuple[int, int]
        The bottom left corner postion of the map.
    generator : CaveMapGeneratorSimple
        The map generator used to build the map.
    """
    TileType = CaveWallTile

    def __init__(self,
                 name: str = "Walls",
                 size: tuple[int, int] = (50, 50),
                 initial_fill_rate: float = 0.6,
                 border_thickness: int = 3,
                 position: tuple[int, int] = (0, 0)):
        super().__init__(
            name,
            CaveMapGeneratorSimple(initial_fill_rate, size, border_thickness),
            size,
            position,
        )

