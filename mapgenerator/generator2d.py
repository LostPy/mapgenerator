"""Base class for 2D map generator.

??? note "License"

    ```
    Copyright © 2022 LostPy <https://gitlab.com/LostPy>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
    ```
"""
from abc import ABC
import random

from . import MapGenerator


class MapGenerator2D(MapGenerator, ABC):
    """Base class for 2D map generator.

    !!! note
        Default `random_fill` method to map with only 1 tile type 
        (+ empty tile).
    """

    def __init__(self,
                 initial_fill_rate: float,
                 map_size: tuple[int, int],
                 border_thickness: int = 0):
        super().__init__(initial_fill_rate, map_size)
        # A number of tiles to set as border of the map
        self.border_thickness: int = border_thickness


    @property
    def total_tiles(self) -> int:
        """Total of tiles in the map/grid.
        
        Returns
        -------
        int : Number of tiles in the map/grid.
        """
        return self.map_size[0] * self.map_size[1]

    def get_surrounding_count(self, x: int, y: int,
                              dx: int = 1, dy: int = 1) -> int:
        """Returns the number of not empty tiles around the given (x, y).

        Returns
        -------
        int : The number of tiles.
        """
        slice_x = slice(x - dx, x + dx)
        slice_y = slice(y - dy, y + dy)
        return sum(
            sum(1 for x in line[slice_x] if x > 0)
            for line in self.map[slice_y]
        )

    def random_fill(self):
        """Fill randomly the grids.
        Initialize `self.map` attribute.
        """
        self.map = [
            [
                1 if x < self.border_thickness
                    or x >= self.map_size[0] - self.border_thickness
                    or y < self.border_thickness
                    or y >= self.map_size[1] - self.border_thickness
                    or random.random() < self.initial_fill_rate
                else 0
                for x in range(self.map_size[0])
            ]
            for y in range(self.map_size[1])
        ]

