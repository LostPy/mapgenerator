"""Implementation of Cave Map generators.

??? note "License"

    ```
    Copyright © 2022 LostPy <https://gitlab.com/LostPy>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
    ```
"""
import random

from . import MapGenerator2D


class CaveMapGeneratorSimple(MapGenerator2D):

    def __init__(self,
                 initial_fill_rate: float,
                 map_size: tuple[int, int], /,
                 border_thickness: int = 3):
        super().__init__(initial_fill_rate,
                         map_size,
                         border_thickness=border_thickness)

    def get_new_tile(self, x, y) -> int:
        """Returns the new tile value according to its neighbouring tiles.
        """
        dx, dy = 2, 2
        if x < dx or y < dy  or x >= self.map_size[0] - dx\
                or y >= self.map_size[1] - dy\
                or x < self.border_thickness\
                or y < self.border_thickness\
                or x >= self.map_size[0] - self.border_thickness\
                or y >= self.map_size[1] - self.border_thickness:
            return 1

        surrounding_tiles = self.get_surrounding_count(x, y, dx=dx, dy=dy)
        if surrounding_tiles  >= 12:
            return 1
        elif surrounding_tiles <= 7:
            return 0
        return self.map[y][x]


    def smooth_map(self):
        """Update the map with a algorithme to smooth the map.
        Assume thaht the map has been initialized.
        """
        self.map = [
            [
                self.get_new_tile(x, y)
                for x in range(self.map_size[0])
            ]
            for y in range(self.map_size[1])
        ]

