"""Some tiles to build a visual map with 
[pygame](https://www.pygame.org).

??? note "License"

    ```
    Copyright © 2022 LostPy <https://gitlab.com/LostPy>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
    ```
"""
from pygame.sprite import Sprite
from pygame import Color, Surface


class Tile(Sprite):
    """Generic tile for a map with pygame.
    To build several tile with differents color or size, create a subclass:

    ```python
    class MyTile(Tile):
        SIZE = 16
        COLOR = pygame.Color("red")
        ...
    ```

    A tile is initialized with the position of bottom left corner:

    ```python
    tile = Tile(0, 0)  # set the tile at the origin of the window (top left corner).
    ```

    Class Attributes
    ----------------
    SIZE : int
        The size of a tile in pixel (SIZE = width = height).
    COLOR : pygame.Color | tuple[int, int, int] | tuple[int, int, int, int]
        The color of a tile defined by a tuple (r, g, b) or (r, g, b, a).
    """

    SIZE: int = 32  # Size in pixel
    COLOR: tuple[int, int, int] = Color("black")

    def __init__(self, x: float, y: float):
        cls = self.__class__
        super().__init__()
        self.image = Surface([cls.SIZE, cls.SIZE])
        self.image.fill(cls.COLOR)
        self.rect = self.image.get_rect()
        self.rect.move_ip(x, y)


class CaveWallTile(Tile):
    """Tile for a cave wall.
    
    To customize the color, just change the class attribute at the 
    initialization of your program.

    ```python
    import pygame
    import mapgenerator


    mapgenerator.pygame.CaveWallTile.COLOR = pygame.Color("red") 

    ...  # your code
    ```
    """
    COLOR = Color("grey27")

