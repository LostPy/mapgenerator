"""Base class for generator.

??? note "License"

    ```
    Copyright © 2022 LostPy <https://gitlab.com/LostPy>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
    ```
"""
from abc import ABC, abstractmethod, abstractproperty


# A list of list fill of integer to represent a grids (map), 
# 2D (`list[list[int]]`) or 3D (`list[list[list[int]]]`).
GenericMap = list


class MapGenerator:
    """Base class for map generator. In this package, a map generator 
    is a class to build a map. This map is represented by a list of 
    list with integer value.

    So, the map is divided into a grid (2D or 3D), an element of the map is 
    a "tile" (2D or 3D).

    !!! note
        Note that the 0 value in grids should be used to represent 
        an empty element (tile).
    """

    def __init__(self, initial_fill_rate: float, map_size: tuple):
        self.initial_fill_rate: float = initial_fill_rate
        self.map_size: tuple = map_size
        self.map: GenericMap = None  # list of list with integer to represent a map

    @property
    def total_tiles(self) -> int:
        """Total of tiles in the map/grid.
        This function can be re-implement in subclass 
        to speed up the computation (to do 1 instruction).
        
        Returns
        -------
        int : Number of tiles in the map/grid.
        """
        r = 1
        for dim in self.map_size:
            r *= dim
        return r

    @abstractmethod
    def random_fill(self):
        """Fill randomly the grids, should be used the `initial_fill_rate` 
        attribute.
        Initialize `self.map` attribute.
        """
        raise NotImplementedError

    @abstractmethod
    def smooth_map(self):
        """Update the map with a algorithme to smooth the map.
        Assume thaht the map has been initialized.
        """
        raise NotImplementedError

    def generate(self, repeat_smoothing: int = 1) -> GenericMap:
        """Initialize and smooth the map then returns the map (not a copy).

        Returns
        -------
        GenericMap (list) : The generated map.
        """
        self.random_fill()
        for _ in range(repeat_smoothing):
            self.smooth_map()
        return self.map

